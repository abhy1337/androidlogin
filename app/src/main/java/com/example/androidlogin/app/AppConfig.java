package com.example.androidlogin.app;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://clutchplate.in/api/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://clutchplate.in/api/register.php";
}